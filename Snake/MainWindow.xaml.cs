﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Snake
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        snakeGame jocSerp = new snakeGame();
        public MainWindow()
        {
            InitializeComponent();
            moviment();
        }

        private void moviment()
        {
            int tamanyXCasella = (int)(canvas.ActualWidth / snakeGame.X_SIZE);
            int tamanyYCasella = (int)(canvas.ActualHeight / snakeGame.Y_SIZE);

            //Redibuixar cada vegada
            Ellipse ellSerp = new Ellipse()
            {
                Fill = Brushes.Pink,
                Width = tamanyXCasella,
                Height = tamanyYCasella
            };
            canvas.Children.Add(ellSerp);

            if (canvas.Children.Count > 3)
            {
                canvas.Children.RemoveRange(0, canvas.Children.Count - 3);
            }

            Canvas.SetTop(ellSerp, jocSerp.CapSerp.Y * tamanyYCasella);
            Canvas.SetLeft(ellSerp, jocSerp.CapSerp.X * tamanyXCasella);
        }

        private void canvas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                jocSerp.moure(DireccioSnake.Avall);
                moviment();
            }
            else if (e.Key == Key.Up)
            {
                jocSerp.moure(DireccioSnake.Amunt);
                moviment();
            }
            else if (e.Key == Key.Right)
            {
                jocSerp.moure(DireccioSnake.Dreta);
                moviment();
            }
            else if (e.Key == Key.Left)
            {
                jocSerp.moure(DireccioSnake.Esquerre);
                moviment();
            }

        }
    }
}
